import { Schema, model } from 'mongoose'
const mongoosePaginate = require('mongoose-paginate-v2')

const UserSchema = new Schema({
  email: { type: String, lowercase: true, trim: true, required: true, unique: true },
  password: { type: String, required: true, select: false },
  firstName: { type: String, required: true },
  lastName: { type: String, required: true },
})
UserSchema.plugin(mongoosePaginate)
export default model('User', UserSchema)
