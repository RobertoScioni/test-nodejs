import { connect } from 'mongoose'
import bodyParser from 'body-parser'
import express from 'express'
import UserModel from './UserModel'
import cors from 'cors'

//#region Configuration
  const pageSize = 5 //pageSize returned by pagination
//#endregion

//#region initialization
  connect(
    'mongodb+srv://test-user:17L2WY9bUWXqQmn3bnfPg8lB@maincluster.kwdmr.gcp.mongodb.net/yakkyofy?retryWrites=true&w=majority',
    { useNewUrlParser: true }
  )
  const app = express()
  app.use(cors())
  app.use(bodyParser.urlencoded({ extended: true }))
  app.use(bodyParser.json())
//#endregion

//#region ROUTES
  /**
   * this looks like a simple pingable route with no real data moving
   */
  app.get('/', (req, res) => {
    res.send('Just a test')
  })

  /**
   * returns a paginated list of all the users
   */
  app.get('/users/page:page/', async (req, res) => {
    try {
      //please not that pagesize is set up at line
      const pagination = { page: req.params.page, limit: pageSize }
      const results = await UserModel.paginate({}, pagination)
      res.send(results)
    } catch (error) {
      res.send('error in paginated get')
      console.error('**************************************************')
      console.error(error)
    }
  })

  /**
   * returns a list of all the users if no particular page is requested
   */
  app.get('/users', async (req, res) => {
    try {
      const results = await UserModel.find()
      res.send(results)
    } catch (error) {
      res.send('error')
      console.error('**************************************************')
      console.error(error)
    }
  })

  /**
   * get a single user
   */
  app.get('/users/:id', async (req, res) => {
    try {
      const user = await UserModel.findById(req.params.id)
      res.send(user)
    } catch (error) {
      console.error('user not found')
      res.status(404).send('user not found')
    }
  })

  /**
   * update a user
   */
  app.put('/users/:id', async (req, res) => {
    try {
      const user = await UserModel.findByIdAndUpdate(req.params.id, req.body)
      res.send(`updated user ${user.firstName} ${user.lastName}`)
    } catch (error) {
      console.error('user not found')
      res.status(404).send('user not found')
    }
  })

  /**
   * delete a user
   */
  app.delete('/users/:id', async (req, res) => {
    try {
      const user = await UserModel.findByIdAndDelete(req.params.id)
      res.send(user)
    } catch (error) {
      console.error('user not found')
      res.status(404).send('user not found')
    }
  })

  /**
   * creates a new user
   */
  app.post('/users', (req, res) => {
    let user = new UserModel()

    user.email = req.body.email
    user.firstName = req.body.firstName
    user.lastName = req.body.lastName
    user.password = req.body.password

    user.save((err, newUser) => {
      if (err) res.send(err)
      else res.send(newUser)
    })
  })
//#endregion
app.listen(8080, () => console.log('Example app listening on port 8080!'))
